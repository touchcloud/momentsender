const lineNotifyClientId = "Qj2vXPtf0r58lAeiup6zLE"
const lineNotifyClientSecret = "FyAZuM9eqjllfJhPMmO40GislYD4zPNBeISRhWquufF"
//Redirect uri must same as line notfiy callback uri
const lineNotifyRedirectUri = "https://script.google.com/macros/s/AKfycbxtMHgn8BHHj3k0cXxg84M2WhabjA1F6KkksGSLwJZA2eLS3Hlcduiu_-cxd-BWuxRE/exec"

let lineNotifyTokens = PropertiesService.getScriptProperties().getProperty("lineNotifyTokens")
if (lineNotifyTokens == null) {
  PropertiesService.getScriptProperties().setProperty("lineNotifyTokens", JSON.stringify([]))
}

function createAuthLink() {
  let data = {
    'response_type': 'code',
    'client_id': lineNotifyClientId,
    'redirect_uri': lineNotifyRedirectUri,
    'scope': 'notify',
    'state': 'state'
  };

  let authorizeURL = "https://notify-bot.line.me/oauth/authorize?response_type=" + data.response_type + "&client_id=" + data.client_id + "&redirect_uri=" + data.redirect_uri + "&scope=" + data.scope + "&state=" + data.state;

  console.log(authorizeURL);

  return authorizeURL
}

function getNotifyLimit() {
  let limits = []

  let lineNotifyTokens = JSON.parse(PropertiesService.getScriptProperties().getProperty("lineNotifyTokens"))
  lineNotifyTokens.forEach(token => {
    let response = UrlFetchApp.fetch('https://notify-api.line.me/api/status', {
      'headers': {
        'Authorization': 'Bearer ' + token,
      },
      'method': 'get',
    });

    let headers = response.getAllHeaders();

    let resetTimestamp = parseInt(headers["x-ratelimit-reset"]) * 1000;

    let time = (new Date(resetTimestamp)).toUTCString();

    let status = {
      "x-ratelimit-limit": headers["x-ratelimit-limit"],
      "x-ratelimit-imagelimit": headers["x-ratelimit-imagelimit"],
      "x-ratelimit-remaining": headers["x-ratelimit-remaining"],
      "x-ratelimit-imageremaining": headers["x-ratelimit-imageremaining"],
      "reset-time(UTC)": time
    };

    console.log(status)

    limits.push(status)
  })

  return limits
}

function pushToken(token) {
  let lineNotifyTokens = JSON.parse(PropertiesService.getScriptProperties().getProperty("lineNotifyTokens"))
  lineNotifyTokens.push(token);
  PropertiesService.getScriptProperties().setProperty("lineNotifyTokens", JSON.stringify(lineNotifyTokens))
}

function popToken(token) {
  let lineNotifyTokens = JSON.parse(PropertiesService.getScriptProperties().getProperty("lineNotifyTokens"))
  let index = lineNotifyTokens.indexOf(token)
  lineNotifyTokens.splice(index, 1)
  PropertiesService.getScriptProperties().setProperty("lineNotifyTokens", JSON.stringify(lineNotifyTokens))
}

function doGet(e) {
  let param = e.parameter;
  let user_code = param.code;
  let state = param.state;

  let data = {
    'grant_type': 'authorization_code',
    'redirect_uri': lineNotifyRedirectUri,
    'client_id': lineNotifyClientId,
    'client_secret': lineNotifyClientSecret,
    'code': user_code,
  };

  let tokenURL = "https://notify-bot.line.me/oauth/token?grant_type=" + data.grant_type + "&redirect_uri=" + data.redirect_uri + "&client_id=" + data.client_id + "&client_secret=" + data.client_secret + "&code=" + data.code;

  let accessToken = null;
  let response = UrlFetchApp.fetch(tokenURL, {
    'method': 'post',
  })

  accessToken = JSON.parse(response.getContentText()).access_token;
  pushToken(accessToken)

  return ContentService.createTextOutput(JSON.stringify(lineNotifyTokens));
}

function doPost(e) {
  let lineNotifyTokens = JSON.parse(PropertiesService.getScriptProperties().getProperty("lineNotifyTokens"))
  let momentObjects = JSON.parse(e.postData.contents);
  let imageUrl = null;

  momentObjects.forEach(object => {
    data = {
      CamID: object.data.CamID,
      CamName: object.data.CamName,
      Time: object.data.Time,
      Item: object.data.Item,
      ItemID: object.data.ItemID,
      Label: object.data.Label,
      Count: object.data.Count,
    };

    let message = "\n" + "攝影機ID: " + data.CamID + "\n" + "攝影機名稱: " + data.CamName + "\n" + "時間(UTC): " + data.Time + "\n" + "物件: " + data.Item + "\n" + "類別: " + data.Label + "\n" + "數量: " + data.Count;
    let snapshot = Utilities.newBlob(Utilities.base64Decode(object.data.snapshot), 'image/png');

    let resource = {
      title: 'Moment Image',
      mimeType: 'image/png'
    };


    let fileResponse = Drive.Files.insert(resource, snapshot);

    let file = DriveApp.getFileById(fileResponse.id);
    file.setSharing(DriveApp.Access.ANYONE_WITH_LINK, DriveApp.Permission.VIEW);
    imageUrl = file;


    console.log(lineNotifyTokens)
    lineNotifyTokens.forEach(token => {
      let response = UrlFetchApp.fetch('https://notify-api.line.me/api/notify', {
        'muteHttpExceptions': true,
        'headers': {
          'Authorization': 'Bearer ' + token,
        },
        'method': 'post',
        'payload': {
          "message": message,
          "imageFile": file.getBlob()
        },
      });

      if (response.getResponseCode() === 401) {
        popToken(token)
      }
    });

    imageUrl = "https://drive.google.com/uc?export=view&id=" + fileResponse.id;
    Drive.Files.remove(fileResponse.id);
  })

  return ContentService.createTextOutput(JSON.stringify(imageUrl));
}

