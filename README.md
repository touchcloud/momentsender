# Description

Touch Cloud momentsender is a program to catch KekkAI endpoint then push to linebot or email

You can enter momentsender and linebot repository to check detail setting

# momentsender_appscript

This is a script for google Apps Script with Line Notify

## How to install

1. Go to [Google App Script](https://script.google.com/home) and create a new project,
   then copy momentsender_appscript.js to project and publish.

- Who can access(誰可以存取) need to select Everybody(所有人)

2. After publish, copy the Web Application URL(網頁應用程式 URL) for line notify callback

3. Go to [Line notify](https://notify-bot.line.me/zh_TW/) and create a new service,

- The email have to fill by valid email for verify
- The Callback URL have to fill by Google App Script's Web Application URL

4. Copy client_id and client_secret to momentsender_appscript's lineNotifyClientId and lineNotifyClientSecret
5. Modify momentsender_appscript's lineNotifyRedirectUri to Web Application URL
6. Click service on Google App Script,find Drive app and add
7. Click publish and select manage publish,then click pencil to edit,and click create new version
8. select createAuthLink function on Google App Script and execute for Line notify auth link
9. Login Line and select witch chat,you want to invite notify
10. Open Line and Add Line Notify official account to chat you select
11. Go to KekkAI endpoint and add Web Application URL to endpoint
