# Momentsender - linebot server

## Description:

Momentsender is Touchcloud's KekkAI endpoint,
and this project is Momentsender's linebot server,
which can distribute Specific LineID, and push notification.

## Requirement:

- a https server domain (line Develop Webhook need, this project use ngrok)
- ngrok (line Develop Webhook need)

- heroku (Line official recommended to build https server domain)

  - if you want to use heroku to build https server domain, you can check below Line tutorial website

  - https://developers.line.biz/zh-hant/docs/messaging-api/building-sample-bot-with-heroku/

## Usage:

```
- cd momentsender
- cd linebot
- ./linebot -secret Channel_secret -token Channel_token
```

- `-secret`: Your LineBot Channel_secret
- `-token`: Your LineBot Channel_token

If you setup server correctly, when you send message on Line, the Cmd line will show 200 OK

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot17.png)

### LineBot Server

#### linebot server Part1:

1. 首先你必須註冊一個官方 Line Develop 帳號,進入 https://developers.line.biz/zh-hant/ ,並登入使用之 Line 帳號(Log in with Line account) ([Pic 1](#Pic1))

2. 建立新的 Provider,機器人名字可自取 ([Pic 2](#Pic2))

3. 建立完後,Channels 處選擇 Create a Messaging API channel ([Pic 3](#Pic3))

4. Channel name, Channel description, Category, Email Address 處可自填,填完,最後兩個同意選項勾選後按下 Create

5. 建立完 Channel 後, Basic settings 中, Channel secret 需記起來供之後 server 啟動所用 ([Pic 4](#Pic4))

6. 接著點選 Messaging API 選項, 其中之 Bot basic ID 及 QRcode 為日後加入 LineBot 時所需 ([Pic 5](#Pic5))

7. 點選 Allow bot to join group chats 旁之 Edit, Group and multi-person chats 改選為 Allow account to join groups and multi-person chats ([Pic 6 7](#Pic6))

8. 點選 Auto-reply messages 旁之 Edit, Auto-response 改選為 Disabled, Webhook 改選為 Enable(Part2 時填寫 Webhook 網址) ([Pic 8 9](#Pic8))

9. 回 Messaging API 選項,最後之 Channel access token 點擊 issue,並記下 token 供之後 server 啟動所用 ([Pic 10](#Pic10))

#### linebot server Part1:

1. First you must register for an official Line Develop account, go to https://developers.line.biz/zh-hant/, and log in to the used Line account (Log in with Line account) ([Pic 1](#Pic1))

2. Create a new Provider, the name of the robot can be self-selected ([Pic 2](#Pic2))

3. After creating, select Create a Messaging API channel at Channels ([Pic 3](#Pic3))

4. Channel name, Channel description, Category, Email Address can be self-filled, after finished, press Create

5. After establishing the Channel, in the basic settings, the Channel secret needs to be remembered for later server startup ([Pic 4](#Pic4))

6. Then click on the Messaging API option, where the Bot basic ID and QRcode are required when adding LineBot in the future ([Pic 5](#Pic5))

7. Click Edit next to Allow bot to join group chats and change Group and multi-person chats to Allow account to join groups and multi-person chats ([Pic 6 7](#Pic6))

8. Click Edit next to Auto-reply messages, change Auto-response to Disabled, Webhook to Enable(fill in Webhook URL when Part2) ([Pic 8 9](#Pic8))

9. Go back to the Messaging API option, click Channel token issue at the end, and note down the token for the server to start later ([Pic 10](#Pic10))

#### Pic1

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot1.png)

#### Pic2

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot2.png)

#### Pic3

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot3.png)

#### Pic4

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot4.png)

#### Pic5

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot5.png)

#### Pic6

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot6.png)

#### Pic7

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot7.png)

#### Pic8

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot8.png)

#### Pic9

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot9.png)

#### Pic10

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot10.png)

### LineBot Server

#### linebot server part2:

LineBot 的 webhook 必須填寫 https 規格之網域,官方建議使用 Heroku 架設 server 網域,本文使用 ngrok 建立簡易網域

1. 首先你必須註冊 ngrok 之帳號,進入 https://ngrok.com/ ,註冊並登入後,在 user 設定 Auth 處找到 Authtoken 並記下來 ([Pic 11](#Pic11))

2. 接著進入 https://ngrok.com/download 跟著官方步驟下載並安裝 ngrok ([Pic 12](#Pic12))

3. 開啟 bash 輸入 ./ngrok authtoken <YOUR_AUTH_TOKEN> (Pic 13) ([Pic 13](#Pic13))

4. 最後輸入 ./ngrok http 8080 (Pic 14) ([Pic 14](#Pic14))

5. 擷取 ngrok 之 Forwarding https 之網址 如 https://6a5e838b.ngrok.io ([Pic 14](#Pic14))

6. 回到 LineBot Messaging API, Webhook URL 處填上 https://6a5e838b.ngrok.io/callback (別忘了 callback) 並按下 Update,即完成 LineBot 與網域之串連 ([Pic 15 16](#Pic15))

#### linebot server part2:

LineBot's webhook must fill in the https domain.Officially, it is recommended to use Heroku to set up the server domain. This project uses ngrok to create a simple domain.

1. First you must register an ngrok account, go to https://ngrok.com/, after registering and logging in, find the Authtoken in the user setting Auth and write it down ([Pic 11](#Pic11))

2. Then go to https://ngrok.com/download Follow the official steps to download and install ngrok ([Pic 12](#Pic12))

3. Open bash and type ./ngrok authtoken <YOUR_AUTH_TOKEN> ([Pic 13](#Pic13))

4. Then type ./ngrok http 8080 ([Pic 14](#Pic14))

5. Retrieve ngrok's Forwarding https URL such as https://6a5e838b.ngrok.io ([Pic 14](#Pic14))

6. Back to the LineBot Messaging API, fill in the Webhook URL https://6a5e838b.ngrok.io/callback (don't miss callback) and press Update to complete the connection between LineBot and the domain ([Pic 15 16](#Pic15))

#### Pic11

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot11.png)

#### Pic12

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot12.png)

#### Pic13

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot13.png)

#### Pic14

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot14.png)

#### Pic15

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot15.png)

#### Pic16

![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/linebot/tutorial/lineBot16.png)

## Notice:

```
如果你是使用Ngrok架設LineBot網域,每當Ngrok重啟一次,則需更改LineBot之Webhook網址

If you use Ngrok to set up a LineBot domain, you need to change the WebHook URL of LineBot whenever Ngrok restarts
```

## FAQ:

```
Q: linebot設置完成,發送訊息給機器人沒反應

A: 請在打開 ngrok 和 linebot Server的情況下,進入 https://developers.line.biz/zh-hant/ ,登入後 Linebot Basic settings 頁面
   點選 Webhook URL選項之Verify嘗試驗證

   1. 如顯示Success則為設置成功,請查看 Part1 8.之 Webhook有無Enable

   2. 如Verify顯示 The webhook returned an HTTP status code other than 200 ,請確認 Webhook網址是否正確
        correct:     https://xxxxxxx.ngrok.io/callback

        wrong:       https://xxxxxx.ngrok.io/
                     http://xxxxxx.ngrok.io/
                     http://xxxxxx.ngrok.io/callback

Q: linebot setup is complete, but sending a message to the robot doesn't respond

A: With ngrok and linebot Server open, go to https://developers.line.biz/zh-hant/ and login to the Linebot Basic settings page
   Click the Verify of Webhook URL option to try to verify

   1. If Success is displayed, the setting is successful, please check whether the Webhook is enabled in Part 1 8.

   2. If Verify shows The webhook returned an HTTP status code other than 200, please confirm that the Webhook URL is correct
        correct:     https://xxxxxxx.ngrok.io/callback

        wrong:       https://xxxxxx.ngrok.io/
                     http://xxxxxx.ngrok.io/
                     http://xxxxxx.ngrok.io/callback
```

```
Q: linebot原本發送訊息都正常,但在使用一段時間後沒了回應

A: 請先查看ngrok 是否有重啟網址更改過,再確認Linebot Server是否正常啟動

    如都正常請查看Linebot頁面之Dashboard流量顯示

    linebot官方預設免費帳號訊息發送量為 500 push/month,如貴公司之linebot超過了此訊息流量,請考慮申請升級為付費版Linebot

    請至Part1 7.點進Edit頁面後,最下方Change plan參考升級價格

Q: Linebot originally sent messages normally, but after a period of time, it didn't respond

A: Please check if ngrok has restart and changed the URL, also confirming that the Linebot Server starts normally

    If everything is normal, please check the Dashboard flow display on Linebot

    Linebot's official default free account message sending volume is 500 push/ month.
    If your company's linebot exceeds this message flow, please consider applying to upgrade to a paid version of Linebot

    Please go to Part 1 7. After clicking into the Edit page, please refer to the upgrade price at the bottom of the Change plan
```
