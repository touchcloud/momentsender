package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/line/line-bot-sdk-go/linebot"
)

type momentRequset struct {
	Message string   `json:"message"`
	ImgUrl  string   `json:"imgUrl"`
	LineID  []string `json:"lineID"`
}

var CHANNEL_SECRET string
var CHANNEL_TOKEN string

var bot *linebot.Client
var err error

var currentDirectory, _ = os.Getwd()

func init() {
	conf = readConfiguration(currentDirectory + "/linebot_conf.json")
}

func lineMessage(context *gin.Context) {
	var momentReq momentRequset
	var in map[string]interface{}

	b, err := ioutil.ReadAll(context.Request.Body)
	if err != nil {
		sugarLogger.Error("Failed to read: ", err)
	}
	defer context.Request.Body.Close()
	err = json.Unmarshal(b, &in)
	if err != nil {
		sugarLogger.Error("Failed to unmarshal: ", err)
	}

	sugarLogger.Info(in)
	limit := checkLinebotLimit(conf.Linebot)
	if _, ok := in["imgUrl"]; ok {
		err = json.Unmarshal(b, &momentReq)
		for _, id := range momentReq.LineID {
			if bot.PushMessage(id, linebot.NewTextMessage(momentReq.Message)).Do(); err != nil {
				sugarLogger.Error("Failed to pushMessage: ", err)
			}
			if checkLinbotRemind(conf.Linebot, limit*7/10, limit*9/10, limit-10) {
				if bot.PushMessage(id, linebot.NewTextMessage("Notice: Total message usage comes to "+strconv.Itoa(checkLinebotTotalUsage(conf.Linebot))+", Your message limit is "+strconv.Itoa(limit))).Do(); err != nil {
					sugarLogger.Error("Failed to pushMessage: ", err)
				}
			}
			if bot.PushMessage(id, linebot.NewImageMessage(momentReq.ImgUrl, momentReq.ImgUrl)).Do(); err != nil {
				sugarLogger.Error("Failed to pushMessage: ", err)
			}
			if checkLinbotRemind(conf.Linebot, limit*7/10, limit*9/10, limit-10) {
				if bot.PushMessage(id, linebot.NewTextMessage("Notice: Total message usage comes to "+strconv.Itoa(checkLinebotTotalUsage(conf.Linebot))+", Your message limit is "+strconv.Itoa(limit))).Do(); err != nil {
					sugarLogger.Error("Failed to pushMessage: ", err)
				}
			}
		}
	} else {
		request := &struct {
			Events []*linebot.Event `json:"events"`
		}{}
		err = json.Unmarshal(b, request)

		for _, event := range request.Events {
			if event.Type == linebot.EventTypeMessage {
				switch event.Message.(type) {
				case *linebot.TextMessage:
					switch event.Source.Type {
					case "user":
						if _, err = bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage("你的ID:"+event.Source.UserID)).Do(); err != nil {
							sugarLogger.Error("Failed to replyMessage: ", err)
						} else {
							sugarLogger.Info("你的ID:" + event.Source.UserID)
						}

					case "group":
						if _, err = bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage("群組ID:"+event.Source.GroupID)).Do(); err != nil {
							sugarLogger.Error("Failed to replyMessage: ", err)
						} else {
							sugarLogger.Info("群組ID:" + event.Source.GroupID)
						}

					}
				}
			}
		}
	}

}

func main() {
	InitLogger()

	bot, err = linebot.New(
		conf.Linebot.Secret,
		conf.Linebot.Token,
	)

	if !checkLineBotField(conf.Linebot) {
		return
	}

	fmt.Println("Linebot server is running...")

	router := gin.Default()
	router.POST("/callback", lineMessage)
	router.Run(":8080")

}
