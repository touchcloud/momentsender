package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

type linebotFormat struct {
	Secret string `json:"secret"`
	Token  string `json:"token"`
}

type configuration struct {
	Linebot linebotFormat `json:"linebot"`
}

type limitRes struct {
	LinebotType string `json:"type"`
	Value       int    `json:"value"`
}

type totalUsageRes struct {
	TotalUsage int `json:"totalUsage"`
}

var conf configuration

func readConfiguration(path string) configuration {

	file, _ := os.Open(path)
	defer file.Close()

	data, _ := ioutil.ReadAll(file)

	conf := configuration{}
	json.Unmarshal(data, &conf)
	return conf
}

func checkLineBotField(linebot linebotFormat) bool {
	if linebot.Secret == "" {
		fmt.Println("You didn't enter Linebot Secret.")
		sugarLogger.Error("Start Linebot server but not input Line Channel Secret")
		return false
	}
	if linebot.Token == "" {
		fmt.Println("You didn't enter Linebot Token.")
		sugarLogger.Error("Start Linebot server but not input Line Channel Token")
		return false
	}
	return true
}

func checkLinebotLimit(linebot linebotFormat) int {
	url := "https://api.line.me/v2/bot/message/quota"
	req, err := http.NewRequest("GET", url, bytes.NewBuffer(nil))
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Set("Authorization", "Bearer "+linebot.Token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		sugarLogger.Error("Failed to read: ", err)
	}

	limitRes := limitRes{}
	err = json.Unmarshal(b, &limitRes)
	if err != nil {
		sugarLogger.Error("Failed to read configuration: ", err)
	}
	return limitRes.Value
}

func checkLinebotTotalUsage(linebot linebotFormat) int {
	url := "https://api.line.me/v2/bot/message/quota/consumption"
	req, err := http.NewRequest("GET", url, bytes.NewBuffer(nil))
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Set("Authorization", "Bearer "+linebot.Token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		sugarLogger.Error("Failed to read: ", err)
	}

	totalUsageRes := totalUsageRes{}
	err = json.Unmarshal(b, &totalUsageRes)
	if err != nil {
		sugarLogger.Error("Failed to read configuration: ", err)
	}
	return totalUsageRes.TotalUsage
}

func checkLinbotRemind(linebot linebotFormat, limits ...int) bool {
	limitNotice := false
	totalUsage := checkLinebotTotalUsage(linebot)
	sugarLogger.Info("Total Usage: ", checkLinebotTotalUsage(linebot))

	for _, limit := range limits {
		if totalUsage == limit {
			limitNotice = true
		}
	}
	return limitNotice
}
