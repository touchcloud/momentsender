package main

import (
	"log"
	"testing"
)

func TestUpload(t *testing.T) {
	imagePath := "./detect.png"
	var imgurl string
	var success bool
	for i := 0; i < 5; i++ {
		imgurl, success = upload(imagePath, accessToken)
		log.Println(imgurl, success)
		if success {
			break
		}
	}
}
