package main

import (
	"encoding/base64"
	"io/ioutil"
	"os"
)

func makeDir(dirPath string) {
	finfo, err := os.Stat(dirPath)
	if err != nil {
		os.Mkdir(dirPath, os.ModePerm)
		sugarLogger.Info("Video Directory is created.\n")
	} else {
		if !finfo.IsDir() {
			os.Mkdir(dirPath, os.ModePerm)
			sugarLogger.Info("Video Directory is created.\n")
		} else {
			sugarLogger.Info("Video Directory has created.\n")
		}
	}
}

func writeAppend(file, data string) {
	f, err := os.OpenFile(file,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		sugarLogger.Error(err)
	}
	defer f.Close()
	if _, err := f.WriteString(data + "\n"); err != nil {
		sugarLogger.Error(err)
	}
}

func IsExists(path string) bool {
	_, err := os.Stat(path)
	if err != nil {
		if os.IsExist(err) {
			return true
		}
		return false
	}
	return true
}

func JpegToBase64(imagePath string) string {
	img, _ := ioutil.ReadFile(imagePath)
	bufstore := base64.StdEncoding.EncodeToString(img)
	return bufstore
}

func Base64ToJpeg(base64Data string, imagePath string) {
	img, _ := base64.StdEncoding.DecodeString(string(base64Data))
	err := ioutil.WriteFile(imagePath, img, 0666)
	if err != nil {
		sugarLogger.Error("Fail convert to Jpeg\n")
	}
}
