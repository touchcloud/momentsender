package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

type lineData struct {
	Message string   `json:"message"`
	ImgUrl  string   `json:"imgUrl"`
	LineID  []string `json:"lineID"`
}

func NewLine(line *lineFormat, message, imgUrl string) lineData {
	return lineData{
		Message: message,
		ImgUrl:  imgUrl,
		LineID:  *line.LineIDs,
	}
}

func checkLineField(line *lineFormat) bool {
	if line != nil {
		if line.LineWebhook != nil && line.LineIDs != nil {
			sugarLogger.Info("Use Line,\nLine Webhook: ", *line.LineWebhook, "\nline IDs: ", *line.LineIDs)
		}
		if line.LineWebhook == nil {
			fmt.Println("You didn't enter line webhook.")
			sugarLogger.Error("Use line sender but not input lineWebhook")
			return false
		} else {
			if strings.Index(*line.LineWebhook, "callback") == -1 {
				fmt.Println("Your lineWebhook format is incorrect, maybe lost callback")
				sugarLogger.Error("Use line sender but not lineWebhook format is incorrect")
				return false
			}
		}
		if line.LineIDs == nil {
			fmt.Println("You didn't enter line IDs.")
			sugarLogger.Error("Use line sender but not input lineIDs")
			return false
		}
	}
	return true
}

func sendToLine(data lineData, line *lineFormat) {
	jsonstr, err := json.Marshal(data)
	if err != nil {
		sugarLogger.Error(err)
	}

	req, err := http.NewRequest("POST", *line.LineWebhook, bytes.NewBuffer(jsonstr))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		sugarLogger.Error(err)
	}
	defer resp.Body.Close()

	sugarLogger.Info("response Status:", resp.Status)
	sugarLogger.Info("response Headers:", resp.Header)
}
