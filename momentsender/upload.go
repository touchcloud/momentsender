package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
)

type imgurData struct {
	Link string `json:"link"`
}

type imgurRes struct {
	Data    imgurData `json:"data"`
	Success bool      `json:"success"`
	Status  int       `json:"status"`
}

var accessToken = "7ce038188f1052ae3b31bdd89c105d86739b230b"

func upload(imagePath, token string) (string, bool) {
	image, err := os.Open(imagePath)
	if err != nil {
		sugarLogger.Error(err)
	}

	var buf = new(bytes.Buffer)
	writer := multipart.NewWriter(buf)

	part, _ := writer.CreateFormFile("image", "dont care about name")
	io.Copy(part, image)

	writer.Close()
	req, _ := http.NewRequest("POST", "https://api.imgur.com/3/image", buf)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	req.Header.Set("Authorization", "Bearer "+token)

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		sugarLogger.Error("Failed to read: ", err)
	}
	defer res.Body.Close()
	b, _ := ioutil.ReadAll(res.Body)

	var imgurres imgurRes
	err = json.Unmarshal(b, &imgurres)
	if err != nil {
		sugarLogger.Error("Failed to get image", err)
	}
	if imgurres.Success {
		return imgurres.Data.Link, imgurres.Success
	} else {
		return string(b), imgurres.Success
	}
}
