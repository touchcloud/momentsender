package main

import (
	"time"
)

func GetTimeNow() time.Time {
	t := time.Now()
	local_location, err := time.LoadLocation("UTC")
	if err != nil {
		sugarLogger.Error(err)
	}
	timeNow := t.In(local_location)
	return timeNow
}

func GetTimeDifferent(timeBefore, timeAfter time.Time) float64 {
	timeDiff := timeAfter.Sub(timeBefore).Seconds()
	return timeDiff
}

func AddTime(baseTime time.Time, seconds int) time.Time {
	timeAfter := baseTime.Add(time.Duration(seconds) * time.Second)
	return timeAfter
}

func ConvertUTCtoLocal(basetime time.Time) time.Time {
	local, err := time.LoadLocation("Local")
	if err != nil {
		sugarLogger.Error(err)
	}
	return basetime.In(local)
}

func ConvertTimetoString(basetime time.Time, format string) string {
	var timeStr string
	timeStr = basetime.Format(format)
	return timeStr
}

func ConvertStringtoTime(timeStr, format string) time.Time {
	baseTime, err := time.Parse(format, timeStr)
	if err != nil {
		sugarLogger.Error(err)
	}
	return baseTime
}
