package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
)

var currentDirectory, _ = os.Getwd()

func init() {
	conf = readConfiguration(currentDirectory + "/momentsender_conf.json")
}

func momentSender(context *gin.Context) {
	context.String(http.StatusOK, "Touchcloud KekkAI Moment")

	b, err := ioutil.ReadAll(context.Request.Body)
	if err != nil {
		sugarLogger.Error("Read failed:", err)
	}
	var in []kekkAI

	err = json.Unmarshal(b, &in)
	sugarLogger.Info(in, "\n\n")

	for _, item := range in {
		imgName := item.Data.Uuid + ".jpg"
		imgPath := currentDirectory + "/" + imgName
		Base64ToJpeg(item.Data.Snapshot, imgPath)
		localTime := ConvertTimetoString(ConvertUTCtoLocal(ConvertStringtoTime(item.Data.Time, "2006-01-02 15:04:05")), "2006-01-02 15:04:05")

		msgContent := "Camera Name:" + item.Data.CamName + "\nItem Name: " + item.Data.Item + "\nLabel: " + item.Data.Label + "\nTime: " + localTime

		if conf.Email != nil {
			if len(*conf.Email.EmailReceivers) > 0 {
				newEmail := NewEmail(conf.Email, msgContent, imgPath)
				sendEmail(conf.Email, newEmail)
			}
		}
		if conf.Line != nil {
			if len(*conf.Line.LineIDs) > 0 {
				var imgurl string
				var success bool
				for i := 0; i < 5; i++ {
					imgurl, success = upload(imgPath, accessToken)
					sugarLogger.Info(imgurl, " ", success)
					if success {
						newLine := NewLine(conf.Line, msgContent, imgurl)
						sendToLine(newLine, conf.Line)
						break
					}
					time.Sleep(time.Duration(3) * time.Second)
				}
			}
		}

		os.Remove(imgPath)
	}
}

func main() {
	InitLogger()
	defer sugarLogger.Sync()

	if !checkEmailField(conf.Email) || !checkLineField(conf.Line) {
		return
	}

	router := gin.Default()
	router.POST("/sender", momentSender)
	router.Run(":8081")
}
