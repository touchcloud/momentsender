package main

import (
	"crypto/tls"
	"errors"
	"fmt"
	"net/smtp"
	"strings"

	"gopkg.in/gomail.v2"
)

type loginAuth struct {
	username, password string
}

func LoginAuth(username, password string) smtp.Auth {
	return &loginAuth{username, password}
}

func (a *loginAuth) Start(server *smtp.ServerInfo) (string, []byte, error) {
	return "LOGIN", []byte{}, nil
}

func (a *loginAuth) Next(fromServer []byte, more bool) ([]byte, error) {
	if more {
		switch string(fromServer) {
		case "Username:":
			return []byte(a.username), nil
		case "Password:":
			return []byte(a.password), nil
		default:
			return nil, errors.New("Unkown fromServer")
		}
	}
	return nil, nil
}

type emailData struct {
	account   *string
	password  *string
	host      string
	receiver  *[]string
	content   string
	imagePath string
}

func NewEmail(email *emailFormat, message, imgPath string) emailData {
	return emailData{
		//host:      "smtp.office365.com",
		host:      "smtp.gmail.com",
		account:   email.EmailAccount,
		password:  email.EmailPassword,
		receiver:  email.EmailReceivers,
		content:   message,
		imagePath: imgPath,
	}
}

func checkEmailField(email *emailFormat) bool {
	if email != nil {
		if email.EmailAccount != nil && email.EmailPassword != nil && email.EmailReceivers != nil {
			sugarLogger.Info("Use Email,\nEmail Account: ", *email.EmailAccount, "\nEmail Password: ", *email.EmailPassword, "\nEmail Receivers: ", *email.EmailReceivers)
		}
		if email.EmailAccount == nil {
			fmt.Println("You didn't enter email account.")
			sugarLogger.Error("Use email sender but not input emailAccount")
			return false
		} else {
			if strings.Index(*email.EmailAccount, "@") == -1 {
				fmt.Println("Your emailAccount format is incorrect, maybe lost @")
				sugarLogger.Error("Use email sender but not emailAccount format is incorrect")
				return false
			}
		}
		if email.EmailPassword == nil {
			fmt.Println("You didn't enter email password.")
			sugarLogger.Error("Use email sender but not input emailPassword")
			return false
		}
		if email.EmailReceivers == nil {
			fmt.Println("You didn't enter email receivers.")
			sugarLogger.Error("Use email sender but not input emailReceivers")
			return false
		} else {
			for _, receiver := range *email.EmailReceivers {
				if strings.Index(receiver, "@") == -1 {
					fmt.Println("Your emailReceiver " + receiver + " format is incorrect, maybe lost @")
					sugarLogger.Error("Use email sender but emailReceiver format is incorrect")
					return false
				}
			}
		}
	}

	return true
}

func seperateAccount(account string) (username, host string) {
	if len(strings.Split(account, "@")) < 2 {
		sugarLogger.Error("Failed to split account")
	} else {
		username = strings.Split(account, "@")[0]
		host = strings.Split(account, "@")[1]
	}
	return username, host
}

func sendEmail(email *emailFormat, e emailData) {
	m := gomail.NewMessage()
	m.SetHeader("From", *e.account)
	m.SetHeader("To", *e.receiver...)
	//m.SetAddressHeader("Cc", "dan@example.com", "Dan")
	m.SetHeader("Subject", "KekkAI Moment")
	m.SetBody("text/html", e.content)
	m.Attach(e.imagePath)

	auth := LoginAuth(*email.EmailAccount, *email.EmailPassword)
	d := gomail.NewDialer(e.host, 587, *e.account, *e.password)
	d.Auth = auth
	d.TLSConfig = &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         e.host,
	}

	// Send the email to Bob, Cora and Dan.
	if err := d.DialAndSend(m); err != nil {
		fmt.Println("1.Please checkout your email account or password is correct.")
		fmt.Println("2.Please check your gmail low secure application access is turn on.")
		sugarLogger.Error(err)
	}

}
