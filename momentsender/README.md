# Outline

- ## [Description](#description-1)

- ## [Installation](#installation-1)

- ## [Usage](#usage-1)

- ## [KekkAI server setup](#KekkAI-server-setup-1)

- ## [FAQ](#faq-1)

- ## [If you want to use email notification](#if-you-want-to-use-email-notification-1)

- ## [If you want to use Line notification](#if-you-want-to-use-line-notification-there-are-two-parts-to-complete-first)

# Introduce:

Momentsender is Touchcloud's KekkAI endpoints.
It can automatically notify KekkAI moment latest information to you by Email & Line.

# Installation:

```
git clone https://gitlab.com/touchcloud/momentsender.git
cd momentsender
docker-compose build
docker-compose up -d
```

# Usage:

```
- cd momentsender
```

- If you only want to use email method

```
modify momentsender_conf.json, new email content.
{
    "email": {
        "emailAccount": "example@gmail.com",
        "emailPassword": "123456",
        "emailReceivers": [
            "example2@gmail.com"
        ]
    },
}
```

- If you only want to use line method

```
modify momentsender_conf.json, new line content.
{
    "line": {
        "lineWebhook": "https://499fddd4d0a7.ngrok.io/callback",
        "lineIDs": [
            "Ucddd301f8328c6762350d19e7ae9b009"
        ]
    }
}
```

- If you want to use both methods

```
modify momentsender_conf.json, new both email and line content.
{
    "email": {
        "emailAccount": "example@gmail.com",
        "emailPassword": "123456",
        "emailReceivers": [
            "example2@gmail.com",
            "example3@gmail.com"
        ]
    },
    "line": {
        "lineWebhook": "https://499fddd4d0a7.ngrok.io/callback",
        "lineIDs": [
            "Ucddd301f8328c6762350d19e7ae9b009",
            "UCddd501568715667d5d5d89d6d3d1568"
        ]
    }
}
```

- `-emailAccount`: Your Gmail Account (Bind to Gmail)
- `-emailPassowrd`: Your Gmail Password
- `-email`: Email Receiver who you want to send mail

- `-lineWebhook`: Your LineBot Webhook
- `-line`: momentsender linebot offer's LineID

* If you're using on docker:

  - cd momentsender
  - docker-compose build
  - docker-compose up -d

## KekkAI server setup:

1. You have to go to http://localhost/ , and enter to Sytem Config
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/KekkAIServer1.png)

2. At KekkAI option, type http://localhost:8081/sender to Url and click Add button
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/KekkAIServer2.png)

3. Finally enable KekkAI,setup and open KekkAI's endpoint to check the data
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/KekkAIServer3.png)

## FAQ:

1. If you encounter any unknowed problem, you can check the momentsender.log by use below command, and check the error code

```
cd momentsender
cat momentsender.log
```

2. If you already setup all thing but the email and line doesn't work, you can check log, whether it show "certificate signed by unknown authority"

   If it is, it because your computer doesn't have certification, try to use below command,and retry the endpoint

```
apt-get update
apt-get install ca-certificates
```

# Notice:

## 如果你想要使用 Email 通知功能

1. 首先你必須登入發信之 Gmail 帳號

2. 點選右上個人圖像,管理你的 google 帳戶
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail1.png)

   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail2.png)

3. 點選左方側欄安全性之選項
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail3.png)

## 如果你未開啟兩步驟驗證功能

4.  低安全性應用程式存取權 設置為開啟
    ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail4.png)

    ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail5.png)

## 如果你已開啟兩步驟驗證功能

4. 點選兩步驟驗證下方之應用程式密碼
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail6.png)

5. 選擇其他應用程式,新增一組應用程式密碼,名稱可自取
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail7.png)

   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail8.png)

6. 記下密碼以供日後 momentsender 程式使用
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail9.png)

**Notice:**日後使用 momentsender 程式之 email 發送功能時,emailPassword 之欄位請使用此十六位應用程式密碼

## If you want to use email notification

1. first you have to go to gmail and login sender mail

2. Click on the top right personal image to manage your google account
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail10.png)

   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail11.png)

3. Click the security option on the left sidebar
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail12.png)

## If you have not enable 2-step verification

4. Less secure app access Set to On
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail13.png)

   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail14.png)

## If you have enable 2-step verification

4. Click on the application password below 2-step verification
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail15.png)

5. Choose type another, add a set of application passwords, the name can be self-named
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail16.png)

   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail17.png)

6. Write down the password for future use by momentsender
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/gmail18.png)

**Notice:** When using the email function of momentsender program in the future, please use this 16-digit application password in the emailPassword field

## 如果你想要使用 Line 通知功能,有兩塊步驟需先完成

### LineBot Server

#### linebot Part:

- 請查看[LineBot](./linebot)子專案資料夾內容

### LineBot Client

1. 在註冊完且確認 LineBot 可正常運作後,加入 LineBot 機器人好友,或將 LineBot 加入至群組內

2. 在對話欄任意發送一句話,LineBot 將回覆屬於你或群組之 KekkAIDemo LineID
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/KekkAILineBot1.png)

3. 在執行 momentsender 之程式時後綴加上 -line LineID 即可發送通知至該 ID

## If you want to use Line notification, there are two parts to complete first

### LineBot Server

#### linebot Part:

- Please look [Linebot](./linebot) subProject directory

### LineBot Client

1. After registering and confirming that LineBot can work normally, join a LineBot robot friend, or add LineBot to a group

2. Send any sentence in the Chat box, LineBot will reply to the momentsender LineID belonging to you or the group
   ![image](https://gitlab.com/touchcloud/momentsender/-/raw/develop/momentsender/tutorial/KekkAILineBot1.png)

3. When running momentsender's program, add the -line LineID suffix that you can send a notification to that ID
