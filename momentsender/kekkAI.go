package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type kekkAI struct {
	DeviceId string `json:"deviceId"`
	Token    string `json:"token"`
	Type     string `json:"type"`
	Data     moment `json:"data"`
}

type moment struct {
	Version  string `json:"Version"`
	Location string `json:"Location"`
	CamID    string `json:"CamID"`
	CamName  string `json:"CamName"`
	Time     string `json:"Time"`
	Item     string `json:"Item"`
	ItemID   string `json:"ItemID"`
	Label    string `json:"Label"`
	Count    int    `json:"Count"`
	TimeZone string `json:"TimeZone"`
	Uuid     string `json:"uuid"`
	Snapshot string `json:"snapshot"`
}

type configuration struct {
	Email *emailFormat `json:"email,omitempty"`
	Line  *lineFormat  `json:"line,omitempty"`
}

type emailFormat struct {
	EmailAccount   *string   `json:"emailAccount,omitempty"`
	EmailPassword  *string   `json:"emailPassword,omitempty"`
	EmailReceivers *[]string `json:"emailReceivers,omitempty"`
}

type lineFormat struct {
	LineWebhook *string   `json:"lineWebhook,omitempty"`
	LineIDs     *[]string `json:"lineIDs,omitempty"`
}

var conf configuration

func readConfiguration(path string) configuration {

	file, _ := os.Open(path)
	defer file.Close()

	data, _ := ioutil.ReadAll(file)

	conf := configuration{}
	err := json.Unmarshal(data, &conf)
	if err != nil {
		sugarLogger.Error("Failed to read configuration: ", err)
	}
	return conf
}
